# newHire
The purpose of this project is to synch Salesforce to Paylocity as far as, each employee in Paylocity should have a  staff campaign and a staff GAU.
There also needs to be an Allocation connecting them.
The main apex class: jsonToCampaign is exposed via the SFDC REST API. 
It recieves a JSON string via a POST to https://(instance).salesforce.com/services/apexrest/Campaign/
There will be a Talend job that reads a database and converts a list of employees to the JSON string and then posts it to this class.
Here is a sample of the JSON string. The order of the fields is not important, but they must all exist and be populated.
	{"hire":[{"fname":"Joe","lname":"Staff","PaylocityId":"1234","email":"joe@sample.com","status":"A"},{"fname":"Jim","lname":"Jones","PaylocityId":"0997","email":"jim@sample.com","status":"A"}]}
The jsonToCampaign class parses the string it receives and Upserts a Campaign to SFDC for each complete record it received.
It then passes that list to CreateGAUfromCamp, which Upserts a General Accounting Unit for each Campaign.
The list of Campaigns and the list of GAUs that were upserted is then passed to the class named CreateAllocation, which creates Allocations.
Finally, jsonToCampaign, sends a list of any incomplete records to the helper class named CampaignToJSON.
This creates a JSON string which is returns to jsonToCampaign, which returns it to the Talend job that posted the request.