/* CampaignToJSON
    Called by: jsonToCampaign
    Tested by: jsonToCampaignTest
    Written by: Tim Osborn
    2019-08-13
*/

public class CampaignToJSON {
    public static String cToJ (List<Campaign> insertedCampaigns){
        String x = '[';
        for (Campaign c : insertedCampaigns){
            x = x + '{"fname":"' + c.fname__c 
            + '","lname":"' + c.lname__c
            + '","PaylocityId":"' + c.PaylocityId__c 
            + '","email":"' + c.staff_email__c
            + '","Status":"' + c.Status 
            +  '"},' ; 
        }
        x = x.removeEnd(',') + ']';
        return x;
    }
}