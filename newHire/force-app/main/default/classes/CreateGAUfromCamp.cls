/* CreateGAUfromCamp
    Called by: jsonToCampaign
    Tested by: jsonToCampaignTest
    Written by: Tim Osborn
    2019-08-13
*/

public class CreateGAUfromCamp {
    public static List<npsp__General_Accounting_Unit__c> CreateGAUfromCamp(List<Campaign> camps) {
        Id ParentGAU = [SELECT Id FROM npsp__General_Accounting_Unit__c WHERE Name = 'IHOPKC 4103 1181' LIMIT 1].Id;
        Id StaffRT = Schema.SObjectType.npsp__General_Accounting_Unit__c.getRecordTypeInfosByDeveloperName().get('Child').getRecordTypeId();
        List<npsp__General_Accounting_Unit__c> GAUs = New List<npsp__General_Accounting_Unit__c>();
        for (Campaign c : camps) {
            npsp__General_Accounting_Unit__c G = new npsp__General_Accounting_Unit__c();
            G.Name = c.fname__c + ' ' + c.lname__c + ' (' + c.PaylocityId__c + ')';
            G.Fund_Designation__c = G.Name;
            G.Fund_Category__c = 'IHOPKC';
            G.PaylocityId__c = c.PaylocityId__c;
            G.npsp__Active__c = c.IsActive;
            G.Parent_GAU__c = ParentGAU;
            G.RecordTypeId = StaffRT;
            GAUs.add(G);
        }

        Schema.SObjectField f = npsp__General_Accounting_Unit__c.Fields.PaylocityId__c;
        Database.UpsertResult[] srList = Database.upsert(GAUs, f, false);
        System.debug('Campaign Save Result is: ' + srList);
        Integer i = 0;
        Integer errs = 0;
        for (Database.UpsertResult sr : srList) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('GAU ' + i + ' failed because: ' + err.getStatusCode());
                    errs++;                    
                }                
            }
        i++;
        }
        if (errs == 0) {
            System.debug('GAUs upserted without errors.');
        } else {
            System.debug('Number of GAUs not created is ' + errs);
        }




        /*try {
            upsert GAUs PaylocityId__c;
            System.debug('GAUs upserted without error.');
        } catch (DmlException e) {
            System.debug('GAU upsert error: ' + e.getMessage());
        } */

        Return GAUs;   
    }
}
