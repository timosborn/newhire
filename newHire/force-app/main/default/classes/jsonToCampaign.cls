/* jsonToCampaign
    Calls the following helper classes:
        CampaignToJSON
        CreateGAUfromCamp
        CreateAllocation
    Tested by: jsonToCampaignTest
    Written by: Tim Osborn
    2019-08-13
*/

@RestResource(urlMapping='/Campaign/*')
global with sharing class jsonToCampaign {
    @HttpPost
    global static String createCampaign() {
        RestRequest req = RestContext.request;
        String st = req.requestBody.toString();

        Id RTId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Staff_Support').getRecordTypeId();
        Campaign p = [SELECT Id FROM Campaign WHERE Name = 'Staff Support' LIMIT 1];
        Campaign c = new Campaign();        
        List<Campaign> camps = new List<Campaign>();
        List<Campaign> incomplete = new List<Campaign>();
                
        //The next two lines can be used to read query parameters from the URL.
        //RestRequest request = RestContext.request;
    	//String st = request.params.get('msg');        
        
        JSONParser parser = JSON.createParser(st);
        Boolean found = false;
        System.JSONToken token;
        String text;       

        //parser.nextToken();		// Eat first START_OBJECT {
        //parser.nextToken();     // Eat token = FIELD_NAME; text = hire
        parser.nextToken();     // Eat first START_ARRAY [
        parser.nextToken();     // Eat the first object's START_OBJECT {

        while((token = parser.nextToken()) != null) {
            // Parse the object
            if ((token = parser.getCurrentToken()) != JSONToken.END_OBJECT) {
                text = parser.getText();
                if (token == JSONToken.FIELD_Name && text == 'fname') {
                    token=parser.nextToken();
                    c.fname__c = parser.getText();
                    if (c.fname__c == '[') {
                        c.fname__c = null;
                    }
                } else if (token == JSONToken.FIELD_Name && text == 'lname') {
                    token=parser.nextToken();
                    c.lname__c = parser.getText();
                    if (c.lname__c == '[') {
                        c.lname__c = null;
                    }
                } else if (token == JSONToken.FIELD_Name && text == 'email') {
                    token=parser.nextToken();
                    c.Staff_Email__c = parser.getText();
                    if (c.Staff_Email__c == '[') {
                        c.Staff_Email__c = null;
                    }
                } else if (token == JSONToken.FIELD_Name && text == 'PaylocityId') {
                    token=parser.nextToken();
                    c.PaylocityId__c = parser.getText();
                    if (c.PaylocityId__c == '[') {
                        c.PaylocityId__c = null;
                    }
                } else if (token == JSONToken.FIELD_Name && text == 'status') {
                    token=parser.nextToken();
                    c.Status = parser.getText();
                } 
            } else {                // Got to an end object so append it to the list
                if (c.fname__c != null && c.lname__c != null && c.Staff_Email__c != null && c.PaylocityId__c != null) {				// Only append to list if we extracted info we wanted.
                    c.Name = 'Ministry of ' + c.fName__c + ' ' + c.lname__c;
                    //c.StartDate = System.today();
                    if (c.Status == 'A') {
                        c.Status = 'In Progress';
                        c.IsActive = true;
                    } else {
                        c.Status = 'Completed';
                        c.IsActive = false;
                    }
                    c.RecordTypeId = RTId;
                    c.ParentId = p.Id;
                    
                    c.StaffInitial__c = c.lname__c.left(2).toUpperCase() + c.fname__c.left(1);
                    camps.add(c);               // add a reference to the list                                             
                } else {
                    incomplete.add(c);
                }
                                    
                // reset and advance to next object
                c = new Campaign();         
                token = parser.nextToken();
                if (token == JSONToken.END_ARRAY) {             // we reached end of array of objects
                    break;
                }
            }
        }
        System.debug('camps =' + camps);
        List<Campaign> camps2 = camps;
        List<npsp__General_Accounting_Unit__c> GAUs = CreateGAUfromCamp.CreateGAUfromCamp(camps2);

        Schema.SObjectField f = Campaign.Fields.PaylocityId__c;
        Database.UpsertResult[] srList = Database.upsert(camps, f, false);
        System.debug('Campaign Save Result is: ' + srList);
        Integer i = 0;
        Integer errs = 0;
        for (Database.UpsertResult sr : srList) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Campaign ' + i + ' failed because: ' + err.getStatusCode());
                    errs++;                    
                }                
            }
        i++;
        }
        if (errs == 0) {
            System.debug('Campaigns upserted without errors.');
        } else {
            System.debug('Number of Campaigns not created is ' + errs);
        }       



        /*try {
            upsert camps PaylocityId__c;
            System.debug('Campaigns upserted without error.');
        } catch (DmlException e) {
            System.debug('Campaign upsert error: ' + e.getMessage());
        } */   
        
        CreateAllocation.CreateAllocation(GAUs, Camps);
        return CampaignToJSON.cToJ(incomplete);
    }
}