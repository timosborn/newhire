/* CreateAllocation
    Called by: jsonToCampaign
    Tested by: jsonToCampaignTest
    Written by: Tim Osborn
    2019-08-13
*/

public class CreateAllocation {
    public static void CreateAllocation(List<npsp__General_Accounting_Unit__c> GAUs, List<Campaign> camps) {
        System.debug('GAUs:' + GAUs);
        System.debug('Campaigns:' + camps);
        Map<String,Id> Gmap = new Map<String,Id>();
        Map<String,Id> Cmap = new Map<String,Id>();
        for (npsp__General_Accounting_Unit__c G : GAUs) {
            if (G.npsp__Active__c) {
                Gmap.put(G.PaylocityId__c, G.Id);
            }            
        }
        for (Campaign C : camps) {
            if (C.IsActive) {
                Cmap.put(C.PaylocityId__c, C.Id);
            }            
        }
        List<npsp__Allocation__c> Allos = new List<npsp__Allocation__c>();
        for (String pid : Gmap.keyset()) {
            npsp__Allocation__c allo = new npsp__Allocation__c();
            allo.npsp__Campaign__c = Cmap.get(pid);
            allo.npsp__General_Accounting_Unit__c = Gmap.get(pid);
            allo.npsp__Percent__c = 100;
            Allos.add(allo);
        } 
        System.debug('Allocations to be created: ' + Allos);        
        Database.SaveResult[] srList = Database.insert(Allos, false);
        System.debug('Save Result is: ' + srList);
        Integer i = 0;
        Integer errs = 0;
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    if (err.getMessage() != 'Percent-based Allocations cannot exceed 100%.') {
                        System.debug('Allocation ' + i + ' failed because: ' + err.getStatusCode());
                        errs++;
                    }
                }                
            }
        i++;
        }
        if (errs == 0) {
            System.debug('Non-existent allocations have been created.');
        } else {
            System.debug('Number of Allocations not created is ' + errs);
        }        
    }
}
