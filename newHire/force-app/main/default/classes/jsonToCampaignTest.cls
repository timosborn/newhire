/* Test Class for the following classes:
    jsonToCampaign
    CampaignToJSON
    CreateGAUfromCamp
    CreateAllocation

    Written by: Tim Osborn
    2019-08-13
*/
@IsTest public with sharing class jsonToCampaignTest {
    @IsTest public static void successfulTest() {
        // Set up a test request for a successful Active, inactive and an incomplete.
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = 'https://cs93.salesforce.com/services/apexrest/Campaign/';
        request.httpMethod = 'POST';
        request.requestBody =  Blob.valueof('[{"lname":"L1","fname":"F1","PaylocityId":"1111","status":"A","email":"first@sample.com"},{"lname":"L2","fname":"F2","PaylocityId":"2222","status":"I","email":"second@sample.com"},{"lname":"L3","fname":"F3","PaylocityId":"3333","status":"A"},{"lname":"[","fname":"[","PaylocityId":"[","status":"[","email":"["}]');
        RestContext.request = request;
        RestContext.response = response;
        Campaign c = new Campaign(Name = 'Staff Support');
        insert c;
        npsp__General_Accounting_Unit__c g = new npsp__General_Accounting_Unit__c(Name = 'IHOPKC 4103 1181', Fund_Designation__c= 'x', npsp__Active__c = true);
        insert g;
        Test.startTest();
            jsonToCampaign.createCampaign();
            Campaign z = [SELECT staff_email__c FROM Campaign WHERE PaylocityId__c = '2222'];
            System.assertEquals('second@sample.com', z.staff_email__c);
            List<npsp__Allocation__c> n = [SELECT npsp__Campaign__r.PaylocityId__c, 
                                         npsp__General_Accounting_Unit__r.PaylocityId__c 
                                    FROM npsp__Allocation__c];
            System.assert(n[0].npsp__Campaign__r.PaylocityId__c == n[0].npsp__General_Accounting_Unit__r.PaylocityId__c);
        Test.stopTest();
    }
    @IsTest public static void unsuccessfulTest() {
        // Set up a test request for an unsuccessful.
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = 'https://cs93.salesforce.com/services/apexrest/Campaign/';
        request.httpMethod = 'POST';
        request.requestBody =  Blob.valueof('{"hire":[{"lname":"L1","fname":"F1","PaylocityId":"1111","status":"A","email":"first@sample.com"},{"lname":"L1","fname":"F1","PaylocityId":"1111","status":"A","email":"first@sample.com"}]}');
        RestContext.request = request;
        RestContext.response = response;
        Campaign c = new Campaign(Name = 'Staff Support');
        insert c;
        npsp__General_Accounting_Unit__c g = new npsp__General_Accounting_Unit__c(Name = 'IHOPKC 4103 1181', Fund_Designation__c= 'x', npsp__Active__c = true);
        insert g;
        Test.startTest();
            jsonToCampaign.createCampaign();
            List<Campaign> l = [SELECT id FROM Campaign];
            System.assertEquals(1, l.size());
        Test.stopTest();
    }
}
